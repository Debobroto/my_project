package com.utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Utility {
	
	public static String[][] getDataFromExcel(String fileName, String sheetName) throws IOException {
		// TODO Auto-generated method stub
		XSSFWorkbook myWorkbook = new XSSFWorkbook(System.getProperty("user.dir") + "//testData//" + fileName);
		XSSFSheet mySheet = myWorkbook.getSheet(sheetName);
		XSSFRow rowHeaders = mySheet.getRow(1); // col names;
		int totalNumberOfColsinTheSheet = rowHeaders.getLastCellNum();		
		int lastRowIndex = mySheet.getLastRowNum();
		
		XSSFRow myRow;
		XSSFCell myCell;
		
		String[][] data = new String[lastRowIndex][totalNumberOfColsinTheSheet];

		for (int row = 1; row <= lastRowIndex; row++) {
			for (int col = 0; col < totalNumberOfColsinTheSheet; col++) {
				myRow = mySheet.getRow(row);
				myCell = myRow.getCell(col);
				data[row - 1][col] = myCell.getStringCellValue();
			}

		}

		return data;
	}

}
