import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BruteForceCode {
	
	public static void main(String[] args) throws EncryptedDocumentException, IOException {
		//1. Launch Browser
		String url = "http://houseofforms.org/ActWiseForms.aspx?Search=&ActID=9650A6D7E5A396D0";
		WebDriverManager.chromedriver().setup();
		WebDriver dr=new ChromeDriver();
		dr.manage().window().maximize();
		dr.get(url);
		
		//2.
		// Load the Excel file
		String filePath = (System.getProperty("user.dir")+"//testData//Document_download_selenium.xlsx");
		Workbook workbook = WorkbookFactory.create(new File(filePath));

		// Select the sheet
		Sheet sheet = workbook.getSheet("Sheet1"); // Replace "Sheet1" with your actual sheet name

		// Get the column containing form names
		int formNameColumnIndex = 1; // Replace with the actual column index

		// Iterate through the rows and search for each form name
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
		    Row row = sheet.getRow(i);
		    Cell cell = row.getCell(formNameColumnIndex);

		    // Get the form name
		    String formName = cell.getStringCellValue().replaceAll("[^\\x20-\\x7E]", "").trim();
		        

		    // Perform the search on the website using the form name
		    WebElement searchInput = dr.findElement(By.id("ctl00_txtSearch")); // Replace "searchInput" with the actual ID of the search input element
		    WebElement searchButton = dr.findElement(By.id("ctl00_Search")); // Replace "searchButton" with the actual ID of the search button element

		    searchInput.clear();
		    searchInput.sendKeys(formName);
		    searchButton.click();
		    WebElement documentLink = dr.findElement(By.xpath("//a[@class='RegularText']")); // Replace "View Document" with the actual link text
		    documentLink.click();
		    WebDriverWait wait = new WebDriverWait(dr, 10);
		    WebElement downloadLink = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("LICENCE TO DRIVE \r\n"
		    		+ ""))); // Replace "Download" with the actual link text

		    
		    
		    
		    // Rest of the code goes here
		}

		
	}

}
