
import java.io.IOException;

import org.apache.poi.*;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
public class ReadExcel {
	
	public static void main(String args[]) throws IOException {
		
		
		
	XSSFWorkbook myWorkbook=new XSSFWorkbook(System.getProperty("user.dir")+"//testData//Document_download_selenium.xlsx");
	XSSFSheet mysheet= myWorkbook.getSheet("Sheet1");
	XSSFRow rowHeader=mysheet.getRow(0);//col names;
	int totalNumberOfColsinTheSheet = rowHeader.getLastCellNum();		
	int lastRowIndex = mysheet.getLastRowNum();	
	
	XSSFRow myRow;
	XSSFCell myCell;
	
	String[][] data = new String[lastRowIndex][totalNumberOfColsinTheSheet];

	for (int row = 1; row <= lastRowIndex; row++) {
		for (int col = 0; col < totalNumberOfColsinTheSheet; col++) {
			myRow = mysheet.getRow(row);
			myCell = myRow.getCell(col);
			data[row - 1][col] = myCell.getStringCellValue();
		}

	}


		
	}

}
